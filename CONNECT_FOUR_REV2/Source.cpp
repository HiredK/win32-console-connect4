﻿#include <iostream>

// WIN32 include
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

// UTILS include
#include "Array2D.h"
#include <cmath>
#include <ctime>

/*/////////////////////////////////////////////////
/// GLOBAL DEFINES:
/////////////////////////////////////////////////*/
/**
 * @def BUILD_PVP_VERSION
 */
//#define BUILD_PVP_VERSION

/**
 * @def FIXED_TIMESTEP_MS
 */
#ifndef FIXED_TIMESTEP_MS
#define FIXED_TIMESTEP_MS 0.01
#endif

/**
 * @def FIXED_CONSOLE_R
 */
#ifndef FIXED_CONSOLE_R
#define FIXED_CONSOLE_R 40
#endif

/**
 * @def FIXED_CONSOLE_C
 */
#ifndef FIXED_CONSOLE_C
#define FIXED_CONSOLE_C 80
#endif

/**
 * @def FIXED_CONNECT_R
 */
#ifndef FIXED_CONNECT_R
#define FIXED_CONNECT_R 6
#endif

/**
 * @def FIXED_CONNECT_C
 */
#ifndef FIXED_CONNECT_C
#define FIXED_CONNECT_C 7
#endif

/*/////////////////////////////////////////////////
/// TYPEDEFS/ENUMS:
/////////////////////////////////////////////////*/
/**
 * @def ColorBit
 */
enum ColorBit
{
	Black           = 0x00,
    DarkBlue        = 0x01,
    DarkGreen       = 0x02,
    DarkRed         = 0x04,

    DarkYellow      = DarkRed | DarkGreen,
    DarkCyan        = DarkGreen | DarkBlue,
    DarkMagneta     = DarkRed | DarkBlue,
	Grey            = DarkRed | DarkGreen | DarkBlue,

    BrightBlue      = DarkBlue  | 0x08,
    BrightGreen     = DarkGreen | 0x08,
    BrightRed       = DarkRed   | 0x08,

    BrightYellow    = BrightRed | BrightGreen,
    BrightCyan      = BrightGreen | BrightBlue,
    BrightMagneta   = BrightRed | BrightBlue,
    White           = BrightRed | BrightGreen | BrightBlue,
};

/**
 * @def GameState
 */
enum GameState
{
	LOGO_SCREEN_STATE = 0,
	GAME_SCREEN_STATE = 1
};

/**
 * @def PlayerValue
 */
enum PlayerValue
{
	DEFAULT_VALUE = 0,
	PLAYER1_VALUE = 1,
	PLAYER2_VALUE = 2
};

/*/////////////////////////////////////////////////
/// GLOBAL MEMBERS: (these MUST be global)
/////////////////////////////////////////////////*/
const char m_logo[] =
{
	" ,-----.                                            ,--.     ,---. "
	"'  .--./  ,---.  ,--,--,  ,--,--,   ,---.   ,---. ,-'  '-.  /    | "
	"|  |     | .-. | |      \\ |      \\ | .-. : | .--' '-.  .-' /  '  | "
	"'  '--'\\ ' '-' ' |  ||  | |  ||  | \\   --. \\ `--.   |  |   '--|  | "
	" `-----'  `---'  `--''--' `--''--'  `----'  `---'   `--'      `--' "
};

Array2D<CHAR_INFO> m_buffer(FIXED_CONSOLE_R,FIXED_CONSOLE_C);
ColorBit m_backColor = Black;
ColorBit m_textColor = White;
HANDLE m_rHnd;
HANDLE m_wHnd;
POINT m_mouse;

/*/////////////////////////////////////////////////
/// GLOBAL SERVICES:
/////////////////////////////////////////////////*/
/**
 * @brief Todo
 */
float GetTime()
{
	static __int64 s = 0;
	static __int64 f = 0;
	if(s == 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&s);
		QueryPerformanceFrequency((LARGE_INTEGER*)&f);
		return 0.f;
	}

	__int64 counter = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&counter);
	return float(((counter-s)/double(f)));
}

/**
 * @brief Todo
 */
void Write(unsigned int r, unsigned int c, wchar_t ch)
{
	assert(m_buffer.valid(r,c));
	m_buffer(r,c).Attributes = (WORD)(m_textColor | (m_backColor << 4));
	m_buffer(r,c).Char.UnicodeChar = ch;
}

/**
 * @brief Todo
 */
void Clear()
{
	for (unsigned int r = 0; r < m_buffer.rows(); r++) {
		for (unsigned int c = 0; c < m_buffer.cols(); c++) {
			Write(r,c,L' ');
		}
	}
}

/**
 * @brief Todo
 */
unsigned int GetLowestRow(PlayerValue board[][FIXED_CONNECT_C], unsigned int c)
{
	for(unsigned int r = 0; r < FIXED_CONNECT_R; r++) {
		if(board[r][c] != DEFAULT_VALUE) {
			return r-1;
		}
	}

	return FIXED_CONNECT_R-1;
}

/**
 * @brief Todo
 */
bool AddToColumn(PlayerValue board[][FIXED_CONNECT_C], unsigned int c, PlayerValue value)
{
	if(board[0][c] == DEFAULT_VALUE) {
		for(unsigned int r = (FIXED_CONNECT_R-1); r >= 0 ; r--) {
			if(board[r][c] == DEFAULT_VALUE) {
				board[r][c] = value;
				return true;
			}
		}
	}

	return false;
}

/**
 * @brief Todo
 */
bool RemFromColumn(PlayerValue board[][FIXED_CONNECT_C], unsigned int c, PlayerValue value)
{
	for(unsigned int r = 0; r < FIXED_CONNECT_R; r++) {
		if(board[r][c] == value)
		{
			board[r][c] = DEFAULT_VALUE;
			return true;
		}
	}
	
	return false;
}

/**
 * @brief Todo
 */
bool CheckForWinner(PlayerValue board[][FIXED_CONNECT_C])
{
	unsigned int start = 0;
	for(unsigned int i = 0; i < FIXED_CONNECT_R; i++)
	for(start = 0; start <= 3; start++)
	{
		/* HORIZONTAL CHECK (-) */
		if((board[i][start] == board[i][start+1] && board[i][start+1] == board[i][start+2] && board[i][start+2] == board[i][start+3]) &&
		   (board[i][start] != 0)) {
			   return true;
		}
	}

	for(unsigned int i = 0; i < FIXED_CONNECT_C; i++)
	for(start = 0; start <= 2; start++)
	{
		/* VERTICAL CHECK (|) */
		if((board[start][i] == board[start+1][i] && board[start+1][i] == board[start+2][i] && board[start+2][i] == board[start+3][i]) &&
		   (board[start][i] != 0)) {
			   return true;
		}
	}
	
	for(unsigned int i = 3; i < FIXED_CONNECT_R; i++)
	for(start = 0; start <= 3; start++)
	{
		/* DIAGONAL CHECK (/) */
		if((board[i][start] == board[i-1][start+1] && board[i-1][start+1] == board[i-2][start+2] && board[i-2][start+2] == board[i-3][start+3]) &&
		   (board[i][start] != 0)) {
			   return true;
		}
	}

	for(unsigned int i = 0; i < 3; i ++)
	for (start = 0; start <= 4; start++)
	{
		/* DIAGONAL CHECK (\) */
		if((board[i][start] == board[i+1][start+1] && board[i+1][start+1] == board[i+2][start+2] && board[i+2][start+2] == board[i+3][start+3]) &&
		   (board[i][start] != 0)) {
			   return true;
		}
	}

	return false;
}

/**
 * @brief Todo
 */
bool CanPlayerWin(PlayerValue board[][FIXED_CONNECT_C], PlayerValue value, unsigned int& move)
{
	bool success = false;
	for(unsigned int c = 0; c < FIXED_CONNECT_C; c++) {
		if(AddToColumn(board,c,value))
		{
			if(CheckForWinner(board)) {
				success = true;
				move = c;
			}

			RemFromColumn(board,c,value);
		}
	}

	return success;
}

/**
 * @brief Todo
 */
bool isBoardFull(PlayerValue board[][FIXED_CONNECT_C])
{
	for(unsigned int c = 0; c < FIXED_CONNECT_C; c++) {
		if(board[0][c] == DEFAULT_VALUE) {
			return false;
		}
	}

	return true;
}

/*/////////////////////////////////////////////////
/// ENTRY POINT:
/////////////////////////////////////////////////*/
/**
 * @brief Todo
 */
int main(int argc, char** argv)
{
	// standard console handles
	m_rHnd = GetStdHandle(STD_INPUT_HANDLE);
	m_wHnd = GetStdHandle(STD_OUTPUT_HANDLE);

	// application state
	bool isRunning = true;
	float now = GetTime();
	float cur = 0.f;

	// global game state
	GameState state = LOGO_SCREEN_STATE;
	unsigned int human = PLAYER1_VALUE;
	unsigned int winnr = DEFAULT_VALUE;
	unsigned int timer = 0;
	unsigned int focus = 0;	

	// global board state
	PlayerValue board[FIXED_CONNECT_R][FIXED_CONNECT_C];
	for(unsigned int i = 0; i < FIXED_CONNECT_R; i++)
	for(unsigned int j = 0; j < FIXED_CONNECT_C; j++) {
		board[i][j] = DEFAULT_VALUE;
	}

	if(m_rHnd && m_wHnd)
	{
		{ /* 1) SETUP WINDOW */
			unsigned int halfX = int(GetSystemMetrics(SM_CXSCREEN) * 0.5) - (FIXED_CONSOLE_C * 4); // 4 default char w
			unsigned int halfY = int(GetSystemMetrics(SM_CYSCREEN) * 0.5) - (FIXED_CONSOLE_R * 6); // 6 default char h

			// center console window
			MoveWindow(GetConsoleWindow(),halfX,halfY,0,0,FALSE);

			// set console window size
			SMALL_RECT rect = { 0, 0, short(FIXED_CONSOLE_C) - 1, short(FIXED_CONSOLE_R) - 1, };
			SetConsoleWindowInfo(m_wHnd, TRUE, &rect);

			// hack to override scroll bar
			COORD size = { short(FIXED_CONSOLE_C), short(FIXED_CONSOLE_R), };
			SetConsoleScreenBufferSize(m_wHnd, size);

			// assign window title
			#ifdef BUILD_PVP_VERSION
				SetConsoleTitle("Connect4 REV2 - PVP");
			#else
				SetConsoleTitle("Connect4 REV2 - PVC");
			#endif
		}

		{ /* 2) MAIN LOOP */
			while(isRunning)
			{
				float tm = GetTime();
				float dt = tm - now;
				now = tm;

				if((cur += dt) >= FIXED_TIMESTEP_MS)
				{
					// seed random value
					srand(unsigned int(time(0)));

					// get mouse position
					GetCursorPos(&m_mouse);
					ScreenToClient(GetConsoleWindow(), &m_mouse);

					// handle console events
					DWORD numEvents, numEventsRead = 0;
					GetNumberOfConsoleInputEvents(m_rHnd, &numEvents);
					if(numEvents != 0)
					{
						INPUT_RECORD* events = new INPUT_RECORD[numEvents];
						ReadConsoleInput(m_rHnd, events, numEvents, &numEventsRead);
						for(DWORD i = 0; i < numEventsRead; i++)
						{
							if(events[i].EventType == MOUSE_EVENT &&
							   events[i].Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED &&
							   state == GAME_SCREEN_STATE)
							{
								if(AddToColumn(board,focus,PlayerValue(human)))
								{
									if(CheckForWinner(board)) {
										if(human == PLAYER1_VALUE)
											winnr = PLAYER1_VALUE;
										else
											winnr = PLAYER2_VALUE;
									}
									else
									{
										#ifdef BUILD_PVP_VERSION
											human = (human == PLAYER1_VALUE) ? PLAYER2_VALUE : PLAYER1_VALUE;
										#else
											unsigned int move;
											do
											{
												if(!CanPlayerWin(board,PLAYER1_VALUE,move) &&
												   !CanPlayerWin(board,PLAYER2_VALUE,move)) {
													move = rand() % FIXED_CONNECT_C;
												}
											}
											while(!AddToColumn(board,move,PLAYER2_VALUE));
											if(CheckForWinner(board)) {
												winnr = PLAYER2_VALUE;
											}
										#endif

										// input beep
										Beep(1000,80);
										Beep(1500,50);
									}
								}
								else
								{
									// error beep
									Beep(1500,50);
									Beep(1000,80);
								}
							}
						}
					}

					Clear();
					switch(state)
					{
						/*/////////////////////////////////////////////////
						/// Logo Screen Update
						/////////////////////////////////////////////////*/
						case LOGO_SCREEN_STATE: {
							m_backColor = White;
							m_textColor = Black;

							if(timer <= 50)
							{
								// print incre transition
								for (unsigned int r = 0; r < m_buffer.rows(); r++)
								for (unsigned int c = 0; c < m_buffer.cols(); c++) {
									if(timer <= 10)
									{
										Write(r,c,L'█');
										continue;
									}
									if(timer <= 20)
									{
										Write(r,c,L'▓');
										continue;
									}
									if(timer <= 30)
									{
										Write(r,c,L'▒');
										continue;
									}
									if(timer <= 40)
									{
										Write(r,c,L'░');
										continue;
									}
								}

								break;
							}

							if(timer >= 150)
							{
								// print decre transition
								for (unsigned int r = 0; r < m_buffer.rows(); r++)
								for (unsigned int c = 0; c < m_buffer.cols(); c++) {
									if(timer >= 190)
									{
										Write(r,c,L'█');
										continue;
									}
									if(timer >= 180)
									{
										Write(r,c,L'▓');
										continue;
									}
									if(timer >= 170)
									{
										Write(r,c,L'▒');
										continue;
									}
									if(timer >= 160)
									{
										Write(r,c,L'░');
										continue;
									}
								}

								if(timer >= 200)
								{
									// process to game state
									state = GAME_SCREEN_STATE;
									m_backColor = Black;
								} break;
							}

							// print upper bar
							m_textColor = DarkCyan;
							for (unsigned int i = 0; i < m_buffer.cols(); i++) {
								Write(16,i,(i%2)?L'▓':L'░');
							}

							// print logo ascii
							m_textColor = DarkBlue;
							unsigned int index = 0;
							for (unsigned int i = 0; i < 5 ; i++)
							for (unsigned int j = 0; j < 67; j++) {
								Write(i+17,j+6,m_logo[index]);
								index++;
							}

							// print lower bar
							m_textColor = DarkCyan;
							for (unsigned int i = 0; i < m_buffer.cols(); i++) {
								Write(22,i,(i%2)?L'▓':L'░');
							} break;
						}

						/*/////////////////////////////////////////////////
						/// Game Screen Update
						/////////////////////////////////////////////////*/
						case GAME_SCREEN_STATE: {
							m_backColor = Black;
							m_textColor = White;

							for(unsigned int r = 0; r <= FIXED_CONNECT_R * 6; r += 6)
							for(unsigned int c = 0; c <= FIXED_CONNECT_C * 6; c += 6)
							{
								unsigned int dr = r +  2;
								unsigned int dc = c + 18;
								if(r<FIXED_CONNECT_R*6 &&
								   c<FIXED_CONNECT_C*6)
								{
									if(m_mouse.x > LONG((dc+1)*8) && m_mouse.x < LONG(dc*8) + (6*8)) {
										if(GetLowestRow(board,(focus = c/6)) == r/6)
										{
											// display lowest row
											m_textColor = (human == PLAYER1_VALUE) ? DarkBlue : DarkRed;
										}

										// print focused col
										for(unsigned int i = 1; i < 6; i++)
										for(unsigned int j = 1; j < 6; j++) {
											Write(dr+i,dc+j,((std::clock()/100)%2)?L'▓':L'▒');
										}
									}

									if(board[r/6][c/6] == PLAYER1_VALUE) {
										m_textColor = BrightBlue;

										// print player1 cell
										for(unsigned int i = 1; i < 6; i++)
										for(unsigned int j = 1; j < 6; j++) {
											Write(dr+i,dc+j,L'█');
										}
									}

									if(board[r/6][c/6] == PLAYER2_VALUE) {
										m_textColor = BrightRed;

										// print player2 cell
										for(unsigned int i = 1; i < 6; i++)
										for(unsigned int j = 1; j < 6; j++) {
											Write(dr+i,dc+j,L'█');
										}
									}

									m_backColor = Black;
									m_textColor = White;
								}

								// print row frames
								if(r < FIXED_CONNECT_R * 6) {
									Write(dr+1,dc,L'║');
									Write(dr+2,dc,L'║');
									Write(dr+3,dc,L'║');
									Write(dr+4,dc,L'║');
									Write(dr+5,dc,L'║');
								}

								// print col frames
								if(c < FIXED_CONNECT_C * 6) {
									Write(dr,dc+1,L'═');
									Write(dr,dc+2,L'═');
									Write(dr,dc+3,L'═');
									Write(dr,dc+4,L'═');
									Write(dr,dc+5,L'═');
								}

								// draw up corners
								if(r == 0 || c == 0) {
									if(r == (FIXED_CONNECT_R * 6)) {
										Write(dr,dc,L'╚');
										continue;
									}
									if(c == (FIXED_CONNECT_C * 6)) {
										Write(dr,dc,L'╗');
										continue;
									}
									if(r % (FIXED_CONNECT_R * 6)) {
										Write(dr,dc,L'╠');
										continue;
									}
									if(c % (FIXED_CONNECT_C * 6)) {
										Write(dr,dc,L'╦');
										continue;
									}
									if(r == 0 && c == 0) {
										Write(dr,dc,L'╔');
										continue;
									}
								}

								// draw low corners
								if(r == (FIXED_CONNECT_R * 6) ||
								   c == (FIXED_CONNECT_C * 6)) {
									if(r % (FIXED_CONNECT_R * 6)) {
										Write(dr,dc,L'╣');
										continue;
									}
									if(c % (FIXED_CONNECT_C * 6)) {
										Write(dr,dc,L'╩');
										continue;
									}
									if(r == (FIXED_CONNECT_R * 6) &&
									   c == (FIXED_CONNECT_C * 6)) {
										Write(dr,dc,L'╝');
										continue;
									}
								}

								// draw joints
								Write(dr,dc,L'╬');
								continue;
							}

						} break;

						default:
						{
						} break;
					}

					// draw back buffer
					COORD size = { m_buffer.cols(), m_buffer.rows() };
					SMALL_RECT rect = { 0, 0, short(m_buffer.cols()) - 1, short(m_buffer.rows()) - 1, };
					WriteConsoleOutputW(m_wHnd, m_buffer.data(), size, COORD(), &rect);
					cur = 0.f;

					// check for winner
					if(winnr != DEFAULT_VALUE) {
						if(winnr == PLAYER1_VALUE)
							printf("PLAYER1 WIN!\n");
						else
							printf("PLAYER2 WIN!\n");

						// winner!
						Beep(1000,50);
						Beep(1500,50);
						Beep(2000,50);
						Beep(2500,80);
						Sleep(2500);
						
						isRunning = false;
					}
					else
					{
						if(isBoardFull(board)) {
							printf("BOARD FULL!\n");
							Sleep(2500);

							isRunning = false;
						}
					}

					timer++;
				}
			}
		}

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}