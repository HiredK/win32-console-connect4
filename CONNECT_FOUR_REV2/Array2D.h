#ifndef ARRAY2D_H
#define ARRAY2D_H

/**
 * @file Array2D.h
 * @brief Todo
 */

#include <cassert>
#include <vector>

template <typename T> class Array2D
{
	public:
		/*/////////////////////////////////////////////////
		/// CTORS/DTORS:
		/////////////////////////////////////////////////*/
		/**
		 * @brief Constructor
		 */
		Array2D(unsigned int rows,
				unsigned int cols)
		{
			init(rows, cols);
		}

		/**
		 * @brief Constructor
		 */
		template <unsigned int r, unsigned int c>
		explicit Array2D(T(&arr)[r][c])
		{
			init(r,c);
			for(unsigned int i = 0; i < r; i++)
			for(unsigned int j = 0; j < c; j++)
			(*this)(i,j) = arr[i][j];
		}

		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * @brief Todo
		 */
		inline bool valid(unsigned int r, unsigned int c) const {
			return r >= 0 && r < rows() && c >= 0 && c < cols();
		}

		/*/////////////////////////////////////////////////
		/// OPERATORS:
		/////////////////////////////////////////////////*/
		/**
		 * @brief Todo
		 */
		inline const T & operator()(unsigned int r, unsigned int c) const {
			assert(valid(r, c));

			return m_data[r * cols() + c];
		}

		/**
		 * @brief Todo
		 */
		inline T & operator()(unsigned int r, unsigned int c) {
			assert(valid(r, c));

			return m_data[r * cols() + c];
		}

		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * @brief Todo
		 */
		inline unsigned int rows() const {
			return m_rows;
		}

		/**
		 * @brief Todo
		 */
		inline unsigned int cols() const {
			return m_cols;
		}

		/**
		 * @brief Todo
		 */
		inline unsigned int size() const {
			return rows() * cols();
		}

		/**
		 * @brief Todo
		 */
		inline const T* data() const {
			return &m_data[0];
		}

	private:
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * @brief Todo
		 */
		void init(unsigned int r,
				  unsigned int c)
		{
			assert(r >= 0 && c >= 0);
			m_rows = r;
			m_cols = c;

			m_data.resize(size());
		}

		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		std::vector<T> m_data; /**< todo */
		unsigned int m_rows; /**< todo */
		unsigned int m_cols; /**< todo */
};

#endif